import Chat from '@/chat/components/Chat.vue';
import ChatInput from '@/chat/components/ChatInput.vue';
import ChatItem from '@/chat/components/ChatItem.vue';
import { shallowMount } from '@vue/test-utils';

describe('Chat', () => {
    function buildWrapper(rollLog = () => []) {
        return shallowMount(Chat, {
            computed: {
                getImage: () => '',
                rollLog,
            },
        });
    }

    it('should have a ChatInput', () => {
        const wrapper = buildWrapper();
        expect(wrapper.contains(ChatInput)).toBeTruthy();
    });

    it('should render chat items', () => {
        const wrapper = buildWrapper(() => [
            {
                id: 'id',
                displayName: 'displayName',
                result: { message: '', elements: [], dice: [] },
                userId: 'userId',
            },
        ]);
        expect(wrapper.contains(ChatItem)).toBeTruthy();
    });

    it('should not repeat username', () => {
        const wrapper = buildWrapper(() => [
            {
                id: '1',
                displayName: 'sender',
                result: { message: '1', elements: [], dice: [] },
                userId: 'senderId',
                timestamp: 1000,
            },
            {
                id: '2',
                displayName: 'sender',
                result: { message: '2', elements: [], dice: [] },
                userId: 'senderId',
                timestamp: 1001,
            },
        ]);
        expect(wrapper.findAll(ChatItem).length).toEqual(1);
    });

    it('should render seperate chat for seperate users', () => {
        const wrapper = buildWrapper(() => [
            {
                id: '1',
                displayName: 'sender01',
                result: { message: '1', elements: [], dice: [] },
                userId: 'senderId01',
                timestamp: 1000,
            },
            {
                id: '2',
                displayName: 'sender02',
                result: { message: '2', elements: [], dice: [] },
                userId: 'senderId02',
                timestamp: 1001,
            },
        ]);
        expect(wrapper.findAll(ChatItem).length).toEqual(2);
    });

    it('should render seperate chat for delays between messages', () => {
        const wrapper = buildWrapper(() => [
            {
                id: '1',
                displayName: 'sender',
                result: { message: '1', elements: [], dice: [] },
                userId: 'senderId',
                timestamp: 100000,
            },
            {
                id: '2',
                displayName: 'sender',
                result: { message: '2', elements: [], dice: [] },
                userId: 'senderId',
                timestamp: 160001,
            },
        ]);
        expect(wrapper.findAll(ChatItem).length).toEqual(2);
    });
});
