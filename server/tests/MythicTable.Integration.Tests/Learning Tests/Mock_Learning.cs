﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace MythicTable.Integration.Tests.Learning_Tests
{
    public class Mock_Learning : IAsyncLifetime
    {
        Mock<Thingamagig> _mockThing;

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        public Task InitializeAsync()
        {
            _mockThing = new Mock<Thingamagig>();

            return Task.CompletedTask;
        }

        public class Thingamagig
        {
            public string Name;
            public virtual string GetName() { return Name; }
            public virtual string GetName(string name) { return name; }
            public virtual Task<string> GetNameAsync() { return Task.FromResult(Name); }
        }

        [Fact]
        public async Task SetupMethod_returnsValue()
        {
            //Arrange
            string startVal = "some string";
            _mockThing.Setup(x => x.GetName()).Returns(startVal);

            //Act
            string returnVal = _mockThing.Object.GetName();

            //Assert
            Assert.Equal(startVal, returnVal);
        }

        [Fact]
        public async Task SetupMethod_ConfigureMethodInput()
        {
            //Arrange
            string startVal = "some string";
            _mockThing.Setup(x => x.GetName(It.IsAny<string>())).Returns(startVal);

            //Act
            string returnVal = _mockThing.Object.GetName("string that shouldn't matter");

            //Assert
            Assert.Equal(startVal, returnVal);
        }

        [Fact]
        public async Task SetupMethod_Async()
        {
            //Arrange
            string startVal = "some string";
            _mockThing.Setup(x => x.GetNameAsync()).Returns(Task.FromResult(startVal));

            //Act
            string returnVal = await _mockThing.Object.GetNameAsync();

            //Assert
            Assert.Equal(startVal, returnVal);
        }
    }
}
