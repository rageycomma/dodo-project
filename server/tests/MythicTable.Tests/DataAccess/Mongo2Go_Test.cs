﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MythicTable.Tests.DataAccess
{
    public class Mongo2Go_Test
    {
        [Fact]
        public void Mongo2Go_Installed()
        {
            //Arrange
            string projectFilePath = Path.Combine(Directory
                .GetParent(Directory.GetCurrentDirectory())
                .Parent
                .Parent
                .Parent
                .Parent
                .Parent
                .FullName
                , "LocalDAOLib"
                , "LocalDAOLib.csproj"
                );
            string mongo2goReference = "<PackageReference Include=\"Mongo2Go\" Version=\"2.2.16\" />";
            bool referenceExists;

            //Act
            if (File.Exists(projectFilePath))
            {
                string fileContents = File.ReadAllText(projectFilePath);
                referenceExists = fileContents.Contains(mongo2goReference);
            }
            else
            {
                referenceExists = false;
                throw new FileNotFoundException($"LocalDAOLib not found at expected: {projectFilePath}");
            }

            //Assert
            Assert.True(referenceExists);
        }
    }
}
