﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Moq;
using MythicTable.Filters;
using Xunit;

namespace MythicTable.Tests.Collections.Services
{
    public class CollectionsPermissionRequired_Test : IAsyncLifetime
    {
        private const string USERNAME = "Jon";
        private const string USERNAME2 = "Karen";

        private string _collection = "123456";
        private ResultExecutingContext _context { get; set; }
        private HttpContext httpContext = new DefaultHttpContext();

        public Task InitializeAsync()
        {
            var mockContext = new Mock<ResultExecutingContext>();
            mockContext.Setup(hc => hc.HttpContext.User.FindFirst(It.IsAny<string>()))
                .Returns(() => new Claim("", USERNAME));

            _context = mockContext.Object;

            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        //[Fact]
        //public async Task OnResultExecutionAsync_CallsNextTask()
        //{
        //    //Arrange
        //    var permissionAttribute = new CollectionsPermissionRequired(_collection, "Get");
        //    HttpContext httpContext = new DefaultHttpContext();
        //    ResultExecutedContext executed = CreateResultExecutedContext(httpContext);

        //    ResultExecutionDelegate resultDelegate = () =>
        //    {
        //        var response = executed.HttpContext.Response;
        //        response.Headers["Content-Type"] = "application/json";
        //        response.Body = new MemoryStream(Encoding.UTF8.GetBytes("{}"));
        //        return Task.FromResult(executed);
        //    };

        //    //Act
        //    await permissionAttribute.OnResultExecutionAsync(_context, resultDelegate);

        //    //Assert
        //}

        //[Fact]
        //public async Task OnResultExecutionAsync_CallsService()
        //{
        //    //Arrange
        //    var permissionAttribute = new CollectionsPermissionRequired(_collection, "Get");


        //    //Act
        //    await permissionAttribute.OnResultExecutionAsync();

        //    //Assert
        //}

        private static ActionContext CreateActionContext(HttpContext context)
        {
            return new ActionContext(context, new RouteData(), new ActionDescriptor());
        }

        private static ResultExecutedContext CreateResultExecutedContext(HttpContext context) =>
            new ResultExecutedContext(CreateActionContext(context), Array.Empty<IFilterMetadata>(), new OkResult(), new object());

        private static ResultExecutingContext CreateResultExecutingContext(HttpContext context) =>
            new ResultExecutingContext(CreateActionContext(context), Array.Empty<IFilterMetadata>(), new OkResult(), new object());


    }
}
