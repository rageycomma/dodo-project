﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Collections.Providers;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xunit;

namespace MythicTable.Tests.Collections.Providers
{
    public class InMemoryCollectionProviderForCampaignsTest
    {
        private const string userId = "test-user";
        private const string secondUserId = "other-user";
        private const string collectionName = "test";
        private const string campaignId = "campaign-test";
        private const string secondCampaignId = "campaign-test2";

        public Mock<ILogger<InMemoryCollectionProvider>> loggerMock;
        private InMemoryCollectionProvider provider;

        public InMemoryCollectionProviderForCampaignsTest()
        {
            loggerMock = new Mock<ILogger<InMemoryCollectionProvider>>();
            provider = new InMemoryCollectionProvider(loggerMock.Object);
        }

        [Fact]
        public async Task CollectionsAreCampaignSpecific()
        {
            var jObject = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());

            Assert.Single(await provider.GetListByCampaign(collectionName, campaignId));
            Assert.Empty(await provider.GetListByCampaign(collectionName, secondCampaignId));
        }

        [Fact]
        public async Task GetAllCombinesCampiagns()
        {
            await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());
            await provider.CreateByCampaign(userId, collectionName, secondCampaignId, new JObject());

            Assert.Equal(2, (await provider.GetList(userId, collectionName)).Count);
        }

        [Fact]
        public async Task CreatesAndGetSingle()
        {
            var jObject = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());
            var found = await provider.GetByCampaign(collectionName, campaignId, jObject.GetId());
            Assert.Equal(jObject.GetId(), found.GetId());
        }

        [Fact]
        public async Task UpdatesJObject()
        {
            var jObject = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());

            var patch = new JsonPatchDocument().Add("foo", "bar");
            var numUpdated = await provider.UpdateByCampaign(collectionName, campaignId, jObject.GetId(), patch);
            Assert.Equal(1, numUpdated);

            var jObjects = await provider.GetListByCampaign(collectionName, campaignId);
            Assert.Single(jObjects);
            Assert.Equal("bar", jObjects[0]["foo"]);
        }

        [Fact]
        public async Task CampaignObjectsAreAccessibleDirectly()
        {
            var created = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());

            var found = await provider.Get(userId, collectionName, created.GetId());

            Assert.NotNull(found);
        }

        [Fact]
        public async Task CampaignObjectsAreCanBeModifiedDirectly()
        {
            var created = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());

            JsonPatchDocument patch = new JsonPatchDocument().Add("foo", "bar");
            var numUpdated = await provider.Update(userId, collectionName, created.GetId(), patch);
            Assert.Equal(1, numUpdated);

            var found = await provider.Get(userId, collectionName, created.GetId());

            Assert.NotNull(found);
            Assert.Equal("bar", found["foo"]);
        }

        [Fact]
        public async Task CampaignObjectsDeletedDirectlyAreRemovedFromCampaign()
        {
            var created = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());

            var numDeleted = await provider.Delete(userId, collectionName, created.GetId());
            Assert.Equal(1, numDeleted);

            Assert.Empty(await provider.GetListByCampaign(collectionName, campaignId));
        }

        [Fact]
        public async Task CampaignObjectsMustBeDeletedByOwner()
        {
            var created = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());

            var numDeleted = await provider.Delete(secondUserId, collectionName, created.GetId());
            Assert.Equal(0, numDeleted);

            Assert.Single(await provider.GetListByCampaign(collectionName, campaignId));
        }

        [Fact]
        public async Task CampaignObjectMisdirectionDeletionBug()
        {
            var created = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());
            await provider.CreateByCampaign(secondUserId, collectionName, secondCampaignId, new JObject());

            var numDeleted = await provider.Delete(secondUserId, collectionName, created.GetId());
            Assert.Equal(0, numDeleted);

            Assert.Single(await provider.GetListByCampaign(collectionName, campaignId));
        }

        [Fact]
        public async Task RecordsOwner()
        {
            var jObject = await provider.CreateByCampaign(userId, collectionName, campaignId, new JObject());
            Assert.Equal(userId, jObject["_userid"]);
        }
    }
}
