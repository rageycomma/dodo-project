﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MythicTable.Notes.Data
{
    /// <summary>
    /// An object for 
    /// </summary>
    public class NoteItemTypeDTO : INoteItemType
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// The name of the note type.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// The description of the type.s
        /// </summary>
        public string TypeDescription { get; set;  }
    }

    /// <summary>
    /// DTO for the note storage.
    /// </summary>
    public class NoteDTO: INoteItem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// The title of the note.
        /// </summary>
        public string NoteTitle {get; set; }

        /// <summary>
        /// The type of the note (i.e. Monster, chapter, module, item, character...)
        /// </summary>
        public INoteItemType NoteType { get; set; }

        /// <summary>
        /// The contents of the note.
        /// </summary>
        public string NoteContents { get; set; }

        /// <summary>
        /// The content type of the note. 
        /// </summary>
        public MythicNoteContentType NoteContentType { get; set; }

        /// <summary>
        /// The tags for this content when being saved.
        /// </summary>
        public List<string> Tags { get; set; }
    }
}
