﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 
/// </summary>
namespace MythicTable.Notes.Exceptions
{
    /// <summary>
    /// The note already exists.
    /// </summary>
    public class NoteAlreadyExistsException: Exception
    {
        /// <summary>
        /// The note already exists, we can't recreate it.
        /// </summary>
        public NoteAlreadyExistsException(): base("The note requested already exists.")
        {

        }
    }

    /// <summary>
    /// The note type requested doesn't exist.
    /// </summary>
    public class NoteTypeDoesNotExistException : Exception
    {
        public NoteTypeDoesNotExistException() : base("The note type requested does not exist")
        {

        }
    }
}
