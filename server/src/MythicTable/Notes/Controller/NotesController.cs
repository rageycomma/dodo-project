﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using MythicTable.Common.Controllers;
using MythicTable.Notes.Data;
using MythicTable.Profile.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MythicTable.Notes.Controller
{
    [Route("noteTypes")]
    public class NotesItemTypeController : AuthorizedController {
        /// <summary>
        /// The provider of the profile information.
        /// </summary>
        private readonly IProfileProvider profileProvider;

        /// <summary>
        /// The provider for note-related items.
        /// </summary>
        private readonly INoteProvider noteProvider;

        /// <summary>
        /// Gets all the 
        /// </summary>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<List<INoteItemType>> ListNoteItemTypes()
        {
            return await this.noteProvider.GetAllNoteTypes();
        }

        /// <summary>
        /// A controller which handles note-item related actions.
        /// </summary>
        /// <param name="profileProvider"></param>
        /// <param name="noteProvider"></param>
        /// <param name="cache"></param>
        public NotesItemTypeController(
            IProfileProvider profileProvider,
            INoteProvider noteProvider,
            IMemoryCache cache
        ) : base(profileProvider, cache)
        {
            this.noteProvider = noteProvider;
            this.profileProvider = profileProvider;
        }
    }


    [Route("notes")]
    public class NotesController: AuthorizedController
    {
        /// <summary>
        /// The profile provider.
        /// </summary>
        private readonly IProfileProvider provider;

        private readonly INoteProvider noteProvider;

        // GET: api/hello
        [HttpGet("hello")]
        public string Hello()
        {
            return "hello";
        }

        [HttpGet("list")]
        public async Task<ActionResult<List<INoteItem>>> getAllNotes()
        {
            return await noteProvider.GetAllNotes();
        }

        /// <summary>
        /// Constructor for the notes controller.
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="cache"></param>
        public NotesController(
            INoteProvider noteProvider,
            IProfileProvider provider,
            IMemoryCache cache
           ) : base(provider, cache)
        {
            this.noteProvider = noteProvider;
            this.provider = provider;
        }
    }
}
