﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using MythicTable.Notes.Exceptions;

/// <summary>
/// The data for notes.
/// </summary>
namespace MythicTable.Notes.Data
{
    /// <summary>
    /// The note-providing thing.
    /// </summary>
    public class InMemoryNoteProvider : INoteProvider
    {
        /// <summary>
        /// The notes the user is storing in memory.
        /// </summary>
        protected List<INoteItem> Notes {get; set; }

        /// <summary>
        /// The note item types.
        /// </summary>
        protected List<INoteItemType> NoteItemTypes { get; set; }

        /// <summary>
        /// Gets the list of all the notes which have been either loaded or created.
        /// The admin should load these.
        /// </summary>
        /// <returns></returns>
        public Task<List<INoteItem>> GetAllNotes()
        {
            return Task.FromResult(Notes);
        }

        /// <summary>
        /// Creates a new note.
        /// </summary>
        /// <param name="incomingNote"></param>
        /// <returns></returns>
        public Task<INoteItem> CreateNote(INoteItem incomingNote)
        {
            // Firstly, we need to check if the note title exists.
            if(!doesNoteExistWithTitle(incomingNote))
            {
                throw new NoteAlreadyExistsException();
            }

            // Does the note type exist?
            if(!doesNoteTypeExist(incomingNote.NoteType))
            {
                throw new NoteTypeDoesNotExistException();
            }

            Notes.Add(incomingNote);
            return Task.FromResult(incomingNote);
        }

        /// <summary>
        /// Gets the type of notes.
        /// </summary>
        /// <returns></returns>
        public Task<List<INoteItemType>> GetAllNoteTypes()
        {
            return Task.FromResult(NoteItemTypes);
        }

        /// <summary>
        /// Does the note type exist?
        /// </summary>
        /// <param name="itemTypeDTO"></param>
        /// <returns></returns>
        protected bool doesNoteTypeExist(INoteItemType itemType)
        {
            return itemType != null && NoteItemTypes.Any(itemType => itemType.Id == itemType.Id || itemType.TypeName == itemType.TypeName);
        }

        /// <summary>
        /// Is there already a note with this title?
        /// </summary>
        /// <param name="NoteTitle"></param>
        /// <returns></returns>
        protected bool doesNoteExistWithTitle(INoteItem noteDTO)
        {
            return Notes.Any<INoteItem>(note => note.NoteTitle == noteDTO.NoteTitle || note.Id == noteDTO.Id);
        }

        /// <summary>
        /// Initialises the notes which are being created.
        /// </summary>
        protected void InitialiseNotes()
        {
            Notes.Add(new NoteDTO()
            {
                Id = Guid.NewGuid().ToString(),
                NoteTitle = "Lost Mines of Phandelver - Chapter 1 - Goblin Arrows",
                NoteType = new NoteItemTypeDTO()
                {
                    TypeName = "Module",
                    TypeDescription = "A game module."
                },
                NoteContentType = MythicNoteContentType.Default_MythicMarkdown,
                NoteContents = "# Dilecta cervix\n\n## Puppe in non sed illud illa formidine\n\n\nLorem markdownum summo poterat: totumque Haec, oculorum atque pectebant **luxsatyri** mille omnes Lycabas pervigilem hasta spectatorem verbaque. Et conatacupidine [avara](http://www.tuo.io/est-per.aspx).\n\nEnim ramos? Nec hic creavit auget divulsere uteroque terricula fuerit robora, etos in primum siquis aut relatu in. Quisque albet venit negate: corna modo necveluti [totaque](http://www.possetcum.net/motae-viscera) lympha **memorata**.Carchesia neque damnosasque ad vale et feror formatur **tamen vel virilia**venerat eat animam.\n\nPuerili diroque vitta mora Meropis fores corpore Pelops umbras sententia, cum.Caede aurigam Thessalidum onus Lernaeae ego usque tuo soror amanti; cum exitus\n\n[senectae](http://vocant-in.net/).\n\nif (template_barebones(screenshotLanManagement, protector_dot) - mask !=\nanimatedOutput) {\ntft_hyperlink(4, intelligenceTelnetCaptcha.goodput(skuCellDcim,\nduplexBacklink), -4 + bar);\nweb_market_kibibyte(memory, chip_minimize, file_access_permalink);\nospf_tag_heuristic.disk_ultra_gigabit = rdramAddressOnline(macintosh) +\npciCommercial;\n}\nlanguageDfs += command;\nif (dragAddressRemote) {\nbaseband = 4 - tagInput;\nmap_google_dashboard = disk + ipPcmcia;\n}\n\n\n## Murmure ante canibusve loquor\n\nPlus in sed sic recessit luctus *genetrixque umentes cervix* suos herbosaquestridentibus pecudes illa hoc! Vincant ter nisi valuisse mihi habuit et**ramosam ter populi** soceri primi, dei qui exsereret partesque locus? Coniugeanno.\n\n1. Gurgite deos tulit\n2. Hic si stuppea sentire sinistra\n3. Caelarat qui precanti dextrae placidissime atque venit\n4. In has illa triplices qui dant Palameden\n\nSe meroque exstante pigneror in insanos dubio ille mea omnibus vidit Remulusquepraesentem maius biformis distantibus metuam partes. Dextra ante tenebat movitvalida bucina transtra, per Acasto. Flamma commissus nunc viris venit usquelauroque amorem si negat figat illa suae hactenus! Rari sidera quaerite et antrodicere ait opus: numina puer puppibus orbem terra, ita tuendos protinus et tibi!Ampyx saepe; non [inque](http://manu.net/nexibustinguebat.html), tellurerespondent ille nunc fraxineam pretium genitalia refert.\n\nNeque rursusque sidus sed at alumnae patefecit frequentat timet, plaustrum,sine? Odoro torum vacuas meminit ea, vir in dum inhibere Troezen traiecit*dignus*. Mentis tenentes et quem incrementa, miserabile naris mediamque tutusnec reddere partes laesasque meministis! Spretis feres vocis Italiae tyranniprius omnia fidemque tum hic altis extemplo sinunt quemquam **in**. In tulitartesque vulnere sume Dorylas.\n\nAdhuc sed Patraeque veneris potest sacrarunt\n[fertur](http://promunturiumquebellis.net/volucres). Lernae iam sideribus estmanifesta umeros tradidit fidemque tempestiva quaque iussit. Submissa sui inquamadopertam mille, exhausta suos indicium incedit, vos rimas, satiata. Nostraresoluta, tamen auro artus dumque petit victum ostendit in bis aestuat rumpo,sola aetas. Et livor.",
                Tags = new List<string>() {
                    "LostMinesOfPhandelver",
                    "GoblinArrows"
                }
            });
        }

        /// <summary>
        /// Hacky as fuck, initialise the note types here just to show what types there are
        /// built-in.
        /// </summary>
        protected void InitialiseNoteTypes()
        {
            NoteItemTypes.Add(new NoteItemTypeDTO()
            {
                Id = Guid.NewGuid().ToString(),
                TypeName = "Module",
                TypeDescription = "A game module, representing an entire adventure."
            });
            NoteItemTypes.Add(new NoteItemTypeDTO()
            {
                Id = Guid.NewGuid().ToString(),
                TypeName = "Chapter",
                TypeDescription = "A game chapter, a part of an entire adventure."
            });
            NoteItemTypes.Add(new NoteItemTypeDTO()
            {
                Id = Guid.NewGuid().ToString(),
                TypeName = "Monster",
                TypeDescription = "A monster, creature, or other."
            });
            NoteItemTypes.Add(new NoteItemTypeDTO()
            {
                Id = Guid.NewGuid().ToString(),
                TypeName = "Person",
                TypeDescription = "A person within a Module, Chapter etc."
            });
            NoteItemTypes.Add(new NoteItemTypeDTO()
            {
                Id = Guid.NewGuid().ToString(),
                TypeName = "Item",
                TypeDescription = "A type of item, consumable, or thing."
            });
            NoteItemTypes.Add(new NoteItemTypeDTO()
            {
                Id = Guid.NewGuid().ToString(),
                TypeName = "Weapon",
                TypeDescription = "A type of offensive item."
            });
        }

        /// <summary>
        /// Creates a new instance of the in-memory note provider.
        /// </summary>
        public InMemoryNoteProvider()
        {
            // Initialise with an empty list.
            Notes = new List<INoteItem>();
            NoteItemTypes = new List<INoteItemType>();
            InitialiseNoteTypes();
            InitialiseNotes();
        }
    }
}
