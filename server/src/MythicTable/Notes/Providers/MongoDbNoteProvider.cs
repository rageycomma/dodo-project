﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MythicTable.Notes.Data
{
    /// <summary>
    /// A note provider for connecting to the MongoDB instance.
    /// </summary>
    public class MongoDbNoteProvider: INoteProvider
    {
        /// <summary>
        /// Gets all the notes.
        /// </summary>
        /// <returns></returns>
        public Task<List<INoteItem>> GetAllNotes()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all the note types.
        /// </summary>
        /// <returns></returns>
        public Task<List<INoteItemType>> GetAllNoteTypes()
        {
            throw new NotImplementedException();
        }
    }
}
