﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Mongo2Go;
using Mongo2Go.Helper;
using MongoDB.Driver;

namespace MythicTable.Collections.Providers
{
    public class MongoDbRunner_Managed : IDisposable
    {
        public string ConnectionString => _runner.ConnectionString;
        public IMongoClient Client { get; internal set; }
     

        private MongoDbRunner _runner;

        private MongoDbRunner_Managed(MongoDbRunner runner)
        {
            _runner = runner;
            AppDomain.CurrentDomain.ProcessExit += DisposeHook;
        }

        ~MongoDbRunner_Managed()
        {
            this.Dispose();
            _runner?.Dispose();
        }

        public static MongoDbRunner_Managed Start()
        {
            MongoDbRunner_Managed runner = new MongoDbRunner_Managed(MongoDbRunner.Start(additionalMongodArguments: "--quiet"));
            
            return runner;
        }

        public void Dispose()
        {
            //ExecuteCommand(@$"taskkill /F /PID {Process.PID} /T");
            _runner?.Dispose();
        }

        private void DisposeHook(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private static void ExecuteCommand(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = System.Diagnostics.Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();
        }
    }
}
