﻿using LocalDAOLib.Models;
using System;
using System.Collections.Generic;

namespace DataAccessLib.DataProviders
{
    public interface IDataAccessObject
    {
        void CreateOrUpdate(object item);
        IEnumerable<object> Get<T>(Func<T, bool> selector) where T:IIndexable;
    }
}