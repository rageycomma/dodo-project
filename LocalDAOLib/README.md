# DataAccessLib Quick Tutorial

## Structure and Responsibilities
- **Database:** The database is responsible for storing and organizing data in an efficient manner. The database may return data in a blob, generic object, or string format, like json or XML.

- **Data Access Object:** The DAO is responsible for interfacing, and querying the database. We use a DAO to control how the DB is accessed.

- **Data Provider:** The data provider is reponsible for interacing the DAO to the rest of the program's POCO objects. When the program needs to retrieve or store POCO objects, a data provider should be requested.

## How to Call a DataProvider<T> with a DI Constructor
Consider the object type needed is `Dollar`, we need a `IDataProvider<Dollar>`. Inside the body of the constructor, a data provider can be retrieved like this:
```csharp
public class MoneyChanger
{
	IDataProvider<Dollar> _dollarProvider;

	public MoneyChanger(IServiceProvider serviceProvider)
	{
		_dollarProvider = (IDataProvider<Dollar>)serviceProvider.GetService(typeof(IDataProvider<Dollar>));
	}
}
```

The resulting action flow should look like this:
```mermaid
flowchart
    direction TB
    1{User Interaction} --> 2[[Web Server]]
    2 -->POCO
    subgraph POCO[POCO Creation Layer]
        direction TB
        DP[FromJObjectDataProvider< T >]
        DO[< T >DTO]
        DP -->|Formats JObject to new T| DO
    end
    DP <---> DA
    DO -.-> 2
    subgraph DA[Data Access]
        direction TB
        MDAO[MongoDAO] <--> Mongo
        subgraph Mongo
            direction TB
            M[["Mongo Server Selector (Selects first available instance)"]]<-.->|Request to the Database|X & Y & Z
            X[[MongoDB Server Cluster Instance]]<-.->D
            Y[[MongoDB Server Cluster Instance]]<-.->D
            Z[[MongoDB Server Cluster Instance]]<-.->D
            D[(MongoDB)]
        end
    end
```
